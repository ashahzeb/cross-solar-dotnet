﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrossSolar.Domain;
using CrossSolar.Models;
using CrossSolar.Repository;
using Microsoft.AspNetCore.Mvc;


namespace CrossSolar.Controllers
{
    [Route("panel")]
    public class AnalyticsController : Controller
    {
        private readonly IAnalyticsRepository _analyticsRepository;

        private readonly IPanelRepository _panelRepository;

        private readonly IDayAnalyticsRepository _dayAnalyticsRepository;

        public AnalyticsController(IAnalyticsRepository analyticsRepository, IPanelRepository panelRepository, IDayAnalyticsRepository dayAnalyticsRepository)
        {
            _analyticsRepository = analyticsRepository;
            _panelRepository = panelRepository;
            _dayAnalyticsRepository = dayAnalyticsRepository;
        }

        // GET panel/XXXX1111YYYY2222/analytics
        [HttpGet("{panelId}/[controller]")]
        public async Task<IActionResult> Get([FromRoute]string panelId)
        {
            Panel p = _panelRepository.Query().FirstOrDefault(x => x.Serial.Equals(panelId, StringComparison.CurrentCultureIgnoreCase));

            if (p == null)
            {
                return NotFound();
            }

            var analytics = await _analyticsRepository.GetByPanelId(p.Id);

            var result = new OneHourElectricityListModel
            {
                OneHourElectricitys = analytics.Select(c => new OneHourElectricityModel
                {
                    Id = c.Id,
                    KiloWatt = c.KiloWatt,
                    DateTime = c.DateTime
                })
            };

            return Ok(result);
        }

        // GET panel/id/analytics/day
        [HttpGet("{panelId}/[controller]/day")]
        public async Task<IActionResult> DayResults([FromRoute]string panelId)
        {
            Panel p = _panelRepository.Query().FirstOrDefault(x => x.Serial.Equals(panelId,StringComparison.CurrentCultureIgnoreCase));

            if (p == null)
            {
                return NotFound();
            }

            List<OneDayElectricityModel> result = await _dayAnalyticsRepository.GetHistoricalDataAsync(p.Id);

            OneDayElectricityListModel resultModel = new OneDayElectricityListModel();
            resultModel.OneDayElectricityModels = result;

            return Ok(resultModel);
        }

        // POST panel/XXXX1111YYYY2222/analytics
        [HttpPost("{panelId}/[controller]")]
        public async Task<IActionResult> Post([FromRoute]string panelId, [FromBody]OneHourElectricityModel value)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Panel p = _panelRepository.Query().FirstOrDefault(x => x.Serial.Equals(panelId));

            if (p == null)
            {
                return NotFound();
            }

            OneHourElectricity oneHourElectricityContent = new OneHourElectricity
            {
                PanelId = p.Id,
                KiloWatt = value.KiloWatt,
                DateTime = DateTime.UtcNow
            };

            await _analyticsRepository.InsertAsync(oneHourElectricityContent);

            OneHourElectricityModel result = new OneHourElectricityModel
            {
                Id = oneHourElectricityContent.Id,
                KiloWatt = oneHourElectricityContent.KiloWatt,
                DateTime = oneHourElectricityContent.DateTime
            };

            return Created($"panel/{panelId}/analytics/{result.Id}", result);
        }
    }
}

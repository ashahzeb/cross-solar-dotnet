﻿using System.ComponentModel.DataAnnotations;

namespace CrossSolar.Models
{
    public class PanelModel
    {
        public int Id { get; set; }

        [Required]
        [Range(-90, 90)]
        [RegularExpression(@"^(\+|-)?(?:90(?:(?:\.0{6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{6})?))$",ErrorMessage ="Latitude must contain 6 decimal places.")]
        public double Latitude { get; set; }

      
        [Range(-180, 180)]
        [RegularExpression(@"^(\+|-)?(?:180(?:(?:\.0{6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{6})?))$", ErrorMessage = "Longitude must contain 6 decimal places.")]
        public double Longitude { get; set; }

        [Required]
        [StringLength(16, MinimumLength = 16, ErrorMessage = "Serial must be 16 characters long")]
        public string Serial { get; set; }

        public string Brand { get; set; }

    }
}
